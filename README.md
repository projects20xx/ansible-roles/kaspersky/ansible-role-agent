Role Install Kaspersky Agent for Linux
====

В данной роли два типа установки KESL-agent:
1. Автономный инсталляционный пакет.
2. Установка через пакет rpm.

**Автономный инсталляционный пакет.**

Автономный инсталляционный пакет создается при помощи KSC.

Устанавливаем скаченный скрипт sh.

Для запуска данного метода, установить переменну в True:

```kesl_agent_autonomous_package: True```

**Установка через пакет rpm.**

Устанавливаем скаченный пакет kesl-agent rpm.

Копируем настройки klnagent.conf.j2.

Применяем настройки klnagent.conf для kesl-agent.

Для запуска данного метода, установить переменну в False:

```kesl_agent_autonomous_package: False```

klnagent_tag_name, используется для автоматического добавления в группу в KSC.

Variables
--------------

kesl_agent_version: "14.0.0-4646"

kesl_agent_distr_url_rpm: 

kesl_agent_manual_package_name: "klnagent64-{{ kesl_agent_version }}.x86_64.rpm" 

kesl_agent_distr_url_sh: 

kesl_agent_autonomous_package_name: "klnagent64-{{ kesl_agent_version }}.x86_64.sh"

kesl_agent_autonomous_package: False

klnagent_server: 

klnagent_udp_port: 15000

klnagent_port: 14000

klnagent_sslport: 13000

klnagent_tag_name: test-stage
